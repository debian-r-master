#!/bin/bash
#set -x
PATH="$PATH:/usr/local/bin"
for dist in squeeze sid; do
    packages_all=""
    sources="Sources_r_${dist}.gz"
    wget -q -O Sources_r_${dist}.gz http://debian-r.int.donarmstrong.com/debian-r/dists/${dist}/main/source/Sources.gz
    wget -q -O Packages_r_all_${dist}.gz http://debian-r.int.donarmstrong.com/debian-r/dists/${dist}/main/binary-all/Packages.gz
    packages_all="Packages_r_all_${dist}.gz"
    if [ "$dist" != "stable" ]; then
	wget -q -O Packages_all_${dist}.gz http://debian.int.donarmstrong.com/debian/dists/${dist}/main/binary-all/Packages.gz
	packages_all="$packages_all Packages_all_${dist}.gz"
    fi;
    for arch in amd64 i386; do
	packages_arch=""
	wget -q -O Packages_r_${arch}_${dist}.gz http://debian-r.int.donarmstrong.com/debian-r/dists/${dist}/main/binary-${arch}/Packages.gz
	packages_arch="$packages_arch Packages_r_${arch}_${dist}.gz"
	wget -q -O Packages_${arch}_${dist}.gz http://debian.int.donarmstrong.com/debian/dists/${dist}/main/binary-${arch}/Packages.gz
	packages_arch="$packages_arch Packages_${arch}_${dist}.gz"
	# wanna-build --verbose -A ${arch} -d ${dist} --merge-v3 Packages_r_all_${dist}.gz Packages_all_${dist}.gz Packages_${arch}_${dist}.gz Packages_r_${arch}_${dist}.gz . Sources_r_${dist}.gz
	wanna-build -A ${arch} -d ${dist} --merge-v3 $packages_all $packages_arch . $sources >/dev/null 2>&1
	rm -f $packages_arch
    done;
    rm -f $packages_all $sources
done;
for arch in amd64 i386; do
#	wanna-build -l build-attempted -A $arch|awk '/_/{print $1}'|awk -F/ '{print $2}'|xargs wanna-build --give-back -A $arch --override >/dev/null 2>&1
	for state in built building build-attempted; do
		wanna-build -l $state -A $arch --min-age 10 |awk '/_/{print $1}'|awk -F/ '{print $2}'|xargs wanna-build --give-back -A $arch --override >/dev/null 2>&1
	done;
done;
